package com.example.h2demo.model;

import javax.persistence.*;


@Entity
public class Person {


    @Id
    private double id;
    private String name;
    private String lastName;

    public Person(double id, String name, String lastName, Adress adress) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.adress = adress;
    }

    public Adress getAdress() {
        return adress;
    }

    public void setAdress(Adress adress) {
        this.adress = adress;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id", referencedColumnName = "id")
    private Adress adress;

    public Person() {

    }

    public Person(double id, String name, String lastName) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
    }


    public double getId() {
        return id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
