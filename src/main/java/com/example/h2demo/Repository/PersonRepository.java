package com.example.h2demo.Repository;

import com.example.h2demo.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, Double> {
}
