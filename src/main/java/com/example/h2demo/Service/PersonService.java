package com.example.h2demo.Service;


import com.example.h2demo.Repository.AdressRepository;
import com.example.h2demo.Repository.PersonRepository;
import com.example.h2demo.model.Adress;
import com.example.h2demo.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class PersonService {

    @Autowired
    PersonRepository personRepository;
    @Autowired
    AdressRepository adressRepository;

    @PostConstruct
    public void init() {

        Adress a = new Adress("Wroclaw", "Nowodworska");
        Person p = new Person(2.2, "Adam", "Nowak");
        p.setAdress(a);


//        personRepository.save(p);
//        adressRepository.save(a);


    }
}
